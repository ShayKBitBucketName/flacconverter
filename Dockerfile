FROM alpine:3.5

ARG INPUT_FILE

RUN apk update && \
    apk add bash && \
    apk add bash-completion && \
    apk add ffmpeg

ADD ${INPUT_FILE} /flac_convertor/

ENTRYPOINT ["/flac_convertor/flac_converter"]
