package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"strings"
)

func main() {
	//path := "/home/rd/Music"
	logLevel := os.Getenv("LOG")
	if len(logLevel) == 0 {
		logLevel = "error"
	} else {
		logLevel = strings.ToLower(logLevel)
	}

	flat := os.Getenv("FLATTEN")
	if len(flat) == 0 {
		flat = "false"
	} else {
		flat = strings.ToLower(flat)
	}
	log.Println("converter started")
	path := "/input_folder"
	outPath := "/output_folder"
	convertDirContent(path, outPath, flat, logLevel)
}

func createFolderIfNotExists(path string) {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		os.Mkdir(path, 0700)
	}
}

func convertDirContent(dir string, outDir string, flatten string, logLevel string) {
	//log.Println(dir)
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		log.Fatal(err)
	}
	for _, f := range files {
		if f.Mode().IsDir() == true {
			if flatten != "true" {
				createFolderIfNotExists(outDir + "/" + f.Name())
			}
			convertDirContent(dir+"/"+f.Name(), outDir, flatten, logLevel)
		} else {
			convertFile(dir+"/"+f.Name(), outDir+"/"+f.Name(), logLevel)
		}
	}
}

func convertFile(filePath string, outPath string, logLevel string) {
	if strings.HasSuffix(filePath, ".flac") {
		outFile := strings.Replace(outPath, ".flac", ".mp3", 1)
		cmdParams := "/usr/bin/ffmpeg" + " -v " + logLevel + " -i \"" + filePath + "\" -ab 320k -map_metadata 0 -id3v2_version 3 \"" + outFile + "\""
		cmd := exec.Command("sh", "-c", cmdParams)
		var out bytes.Buffer
		var err bytes.Buffer
		cmd.Stdout = &out
		cmd.Stderr = &err
		error := cmd.Run()
		if error != nil {
			log.Println("failed converting file " + filePath)
			log.Println(error)
		}
		if out.Len() > 0 {
			fmt.Printf("%q\n", out.String())
		}
		if err.Len() > 0 {
			fmt.Printf("%q\n", err.String())
		}
		log.Println("finished " + filePath)
	}
}
